package model

import (
	// "fmt"

	// "p24test/app/constant"
	"backend/app/utils"

	// "log"
	// "time"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"gorm.io/gorm"
)

type Account struct {
	ID            int    		`gorm:"primary_key" json:"-"`
	Fullname      string	 	`json:"fullname,omitempty"`
	Email         string 		`json:"email"`
	Password      string 		`json:"password,omitempty"`
	HP		      int    		`json:"handphone,omitempty"`
	Address       string    	`json:"address"`
}

type Auth struct {
	Email     string `json:"email"`
	Password string `json:"password"`
}

func Login(auth Auth) (bool, error, string) {
	var account Account
	if err := DB.Where(&Account{Email: auth.Email}).First(&account).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, errors.Errorf("Account not found"), ""
		}
	}

	err := utils.HashComparator([]byte(account.Password), []byte(auth.Password))
	if err != nil {
		return false, errors.Errorf("Incorrect Password"), ""
	}else{
		sign := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"email":           auth.Email,
			"id": 				  account.ID,
		})

		token, err := sign.SignedString([]byte("secret"))
		if err != nil {
			return false, err, ""
		}
		return true, nil, token
	}
}

func InsertNewAccount(account Account) (bool, error) {
	if err := DB.Create(&account).Error; err != nil {
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	return true, nil
}